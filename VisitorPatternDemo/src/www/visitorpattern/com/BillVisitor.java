/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.visitorpattern.com;

/**
 *
 * @author Danny
 */
public interface BillVisitor {
    void visit(ElectricCompany ec);
    void visit(GasCompany gc);
    void visit(WaterCompany wc);
    void visit(TelephoneCompany tc);
    void visit(BillManager bm);
}
